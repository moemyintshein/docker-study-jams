# docker-study-jams

Docker Study Jams - Demo

# Topics

1.  Using Auto DevOps in gitlab
2.  Creating custom pipeline in gitlab
3.  Building Docker images in gitlab ci-cd pipeline
4.  Integrating kubertetes with gitlab
5.  Deploy sample project to kubernetes

### Intro

[Runners](https://docs.gitlab.com/ee/ci/runners/README.html)

http://bit.ly/2XWjmZ8


1.  Shared Runner
2.  Group Runner
3.  Specific Runner

### 1.  Using Auto DevOps in gitlab

Settings -> CI / CD -> Auto DevOps -> Default to Auto DevOps pipeline (check)

[Audo DevOps Details](https://gitlab.com/help/topics/autodevops/index.md)

[Heroku supported Buildpack](https://devcenter.heroku.com/articles/buildpacks#officially-supported-buildpacks)

### 2. Creating custom pipeline
Create .gitlab-ci.yml
Example -
```
stages:
  - composer install
  - docker build

composer:install:
  image: composer
  stage: composer install
  script:
  - cd src && composer install
  artifacts:
    paths:
    - src/

docker:image:
  stage: docker build
  image: docker
  only:
    - tags
  services:
    - docker:dind
  script:
    - docker version
    - docker build -t $CI_REGISTRY_IMAGE:latest .
    # push only for tags
    - docker tag $CI_REGISTRY_IMAGE:latest $CI_REGISTRY_IMAGE:$CI_BUILD_TAG
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$CI_BUILD_TAG
  dependencies:
    - composer:install

```

### 4.  Integrating kubertetes with gitlab

Operations -> Kubernetes

https://gitlab.com/help/user/project/clusters/index#adding-an-existing-kubernetes-cluster


### 5.  Deploy sample project to kubernetes

Create deploy token

Settings -> Repository -> Deploy Token

add variables
Settings -> CI/CD -> Variables

$CI_DEPLOY_USER, $CI_DEPLOY_PASSWORD

```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com/$CI_PROJECT_PATH_SLUG --docker-username=$CI_DEPLOY_USER --docker-password=$CI_DEPLOY_PASSWORD  --docker-email=$GITLAB_USER_EMAIL
kubectl create service loadbalancer lumen-docker --tcp=80:80 -o yaml --dry-run | kubectl apply -f -
kubectl apply -f docs/kubernetes/deployment.yaml
```

Deployment.yaml
```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: lumen-docker
  name: lumen-docker
spec:
  replicas: 1
  selector:
    matchLabels:
      app: lumen-docker
  template:
    metadata:
      labels:
        app: lumen-docker
    spec:
      containers:
      - image: registry.gitlab.com/herzcthu/lumen-docker:v1.0.0
        imagePullPolicy: Always
        name: lumen-docker
      imagePullSecrets:
      - name: regcred
  
```

# References
1.  https://docs.gitlab.com/ee/ci/pipelines.html
2.  https://gitlab.com/herzcthu/lumen-docker
3.  https://datasift.github.io/gitflow/IntroducingGitFlow.html